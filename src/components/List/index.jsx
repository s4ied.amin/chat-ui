import React from "react";
import css from "./list.module.css";
import { Avatar, Badge } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

const List = (props) => {
  const { usersList, onClick } = props;

  const listId = Object.keys(usersList);

  return (
    <ul className={css.list}>
      {listId.map((id) => {
        const user = usersList[id];
        return (
          <li key={user.id}>
            <Link
              to={`/user/${user.id}`}
              onClick={() => (onClick ? onClick(user.id) : "")}
            >
              <Avatar
                className={css.avatar}
                size={50}
                icon={<UserOutlined />}
              />
              <div className={css.info}>
                <div>{user.name}</div>
                <div className={css.lastMessage}>
                  {user.lastMessage ? user.lastMessage.content : ""}
                </div>
              </div>
              <div className={css.status}>
                <div className={css.time}>
                  {user.lastMessage ? user.lastMessage.time : ""}
                </div>
                <Badge count={user.unreadMessage ? user.unreadMessage : ""} />
              </div>
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

export default List;
