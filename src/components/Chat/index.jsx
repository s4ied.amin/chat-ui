import React, { useState, useEffect, useRef, useContext } from "react";
import css from "./chat.module.css";
import Message from "../Message";
import { Avatar, Drawer, Row, Col, Button } from "antd";
import User from "../User";
import { timeStamp } from "../../utils/time";
import UsersContext from "../../store";
import { Link } from "react-router-dom";

import { SendOutlined, UserOutlined, CloseOutlined } from "@ant-design/icons";

const Chat = (props) => {
  const { userId } = props;

  const { updateUserStatus } = useContext(UsersContext);

  // for performance boost there is a refrence to historyChat
  // so in cleanup we only once stringify and write in storage
  const historyRef = useRef({});

  const bodyEnd = useRef(null);
  const [visible, setVisibile] = useState(false);
  const [msg, setMsg] = useState("");
  const [historyChat, setHistoryChat] = useState([]);

  const closeHandle = (e) => {
    setVisibile(false);
  };
  const inputChange = (e) => {
    setMsg(e.target.value);
  };

  const autoResponse = () => {
    // auo response
    setTimeout(() => {
      const messageObj = {
        sent: false,
        text: "dummy response",
        time: timeStamp(),
      };
      setHistoryChat((state) => [...state, messageObj]);
      updateUserStatus({
        id: userId,
        lastMessage: {
          content: "dummy response",
          time: timeStamp(),
        },
        unreadMessage: 1,
      });
      // by changing this timeout and returning to user list
      // u can see the notification
    }, 2000);
  };

  const onSend = (e) => {
    const messageObj = {
      sent: true,
      text: msg.trim(),
      time: timeStamp(),
    };
    setHistoryChat((state) => [...state, messageObj]);

    setMsg("");

    autoResponse();
  };

  useEffect(() => {
    const chats = JSON.parse(sessionStorage.getItem(userId));
    if (chats && chats.length) setHistoryChat(chats);

    // cleanup function
    return () => {
      const historyString = JSON.stringify(historyRef.current);
      sessionStorage.setItem(userId, historyString);

      if (historyRef.current.length) {
        const lastMsg = historyRef.current[historyRef.current.length - 1];
        // update the last message on user list
        updateUserStatus({
          id: userId,
          lastMessage: {
            content: lastMsg.text,
            time: lastMsg.time,
          },
          unreadMessage: 0,
        });
      }

      setHistoryChat([]);
    };
  }, []);

  useEffect(() => {
    // always be at bottom of the chat history
    bodyEnd.current.scrollIntoView(true);
    historyRef.current = historyChat;
    updateUserStatus({ id: userId, unreadMessage: 0 });
  }, [historyChat]);

  return (
    <div className={css.chatWrapper}>
      <div className={css.header}>
        <Button type="link">
          <Link to="/">
            <CloseOutlined className={css.return} />
          </Link>
        </Button>
        <a onClick={(e) => setVisibile(true)}>
          <Avatar className={css.avatar} size={50} icon={<UserOutlined />} />
        </a>
      </div>
      <Drawer
        visible={visible}
        getContainer={false}
        width="100%"
        closable={false}
        style={{ position: "absolute" }}
        bodyStyle={{ padding: "0", backgroundColor: "#282c34" }}
      >
        <User onClose={closeHandle} userId={userId}/>
      </Drawer>
      <div className={css.body}>
        <div className={css.container}>
          {historyChat.length
            ? historyChat.map((item, i) => (
                <Message key={i} sent={item.sent} time={item.time}>
                  {item.text}
                </Message>
              ))
            : ""}
          <span ref={bodyEnd} />
        </div>
      </div>
      <Row className={css.footer} align="middle">
        <Col flex="50px">
          <Avatar className={css.avatar} size={50} icon={<UserOutlined />} />
        </Col>
        <Col flex="auto">
          <textarea
            rows={3}
            placeholder="your message"
            onChange={inputChange}
            value={msg}
          ></textarea>
        </Col>
        <Col flex="50px">
          <Button type="link" disabled={!msg}>
            <SendOutlined
              rotate={-45}
              className={css.sentBtn}
              onClick={onSend}
            />
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default Chat;
