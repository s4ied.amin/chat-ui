import React, { useContext } from "react";
import css from "./App.module.css";
import List from "../../components/List";
import UsersContext from "../../store";
import { Button } from "antd";
import { UserAddOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

function App() {
  const usersList = useContext(UsersContext).users;

  return (
    <div className={css.App}>
      <Button type="link" className={css.contactLink}>
        <Link to="/contacts">
          <UserAddOutlined className={css.add} />
          Contacts
        </Link>
      </Button>

      <List usersList={usersList} />
    </div>
  );
}

export default App;
