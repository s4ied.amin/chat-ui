import React from "react";
import Chat from "../../components/Chat";

const User = (props) => {
  const {params} = props.match
  return (
    <div>
      <Chat userId={params.id} />
    </div>
  );
};

export default User;
