import React, { Suspense, lazy, useState } from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter as Router } from "react-router-dom";
import "antd/dist/antd.css";
import "./index.css";
import Theme from "./theme";
import App from "./pages/App";
import Contacts from "./pages/Contacts";
// import User from "./pages/User";
import * as serviceWorker from "./serviceWorker";
import UsersContext from "./store";
import { timeStamp } from "./utils/time";

// code spiliting on large page or section
const User = lazy(() => import("./pages/User"));

const initiate = () => {
  sessionStorage.setItem(
    "1",
    '[{"sent":true,"text":"hello","time":"4:40"},{"sent":false,"text":"dummy response","time":"4:40"},{"sent":true,"text":"test the response","time":"4:40"}]'
  );

  return {
    1: {
      id: 1,
      name: "john",
      lastMessage: {
        content: "test the response",
        time: "4:40",
      },
      unreadMessage: 0,
    },
    2: {
      id: 2,
      name: "McG",
      lastMessage: {
        content: "",
        time: timeStamp(),
      },
      unreadMessage: 0,
    }
  }
};

const defaultList = initiate();

const AppRouting = () => {
  const [users, setUsers] = useState(defaultList);

  const updateUserStatus = (user) => {
    const newState = { ...users };

    if (!newState[user.id])
      newState[user.id] = { id: user.id, name: user.name };

    if (user.lastMessage) {
      newState[user.id].lastMessage = user.lastMessage;
    }
    if (user.unreadMessage >= 0) {
      newState[user.id].unreadMessage = user.unreadMessage;
    }
    setUsers(newState);
  };

  return (
    <Theme>
      <UsersContext.Provider value={{ users, updateUserStatus }}>
        <Router>
          <Suspense fallback={<div>loading ...</div>}>
            <Route exact path="/" component={App} />
            <Route path="/user/:id" component={User} />
            <Route path="/contacts" component={Contacts} />
          </Suspense>
        </Router>
      </UsersContext.Provider>
    </Theme>
  );
};

ReactDOM.render(<AppRouting />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

