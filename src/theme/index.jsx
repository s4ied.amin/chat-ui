import React from "react";
import { Layout } from "antd";
import css from "./theme.module.css";

const { Content } = Layout;

const Theme = (props) => {
  return (
    <Layout style={{backgroundColor: "black"}}>
      <Content className={css.themeBody}>
        <div>{props.children}</div>
      </Content>
    </Layout>
  );
};

export default Theme;
